<?php
/**
 * Author: My Nguyen Thi Chi
 */

class Circle
{
    const PI = 3.14;

    private $radius;

    public function getRadius()
    {
        return $this->radius;
    }

    public function setRadius($radius)
    {
        $this->validateRadius($radius);
        $this->radius = $radius;
    }

    private function validateRadius($radius)
    {
        $radius = filter_var($radius, FILTER_VALIDATE_FLOAT);
        if ($radius === false || $radius < 0) {
            throw new Exception('The radius is invalid.');
        }
    }

    public function inputRadius()
    {
        $input = readline('Enter the radius: ');
        $this->setRadius($input);
    }

    public function getArea()
    {
        return round(pow($this->radius, 2) * self::PI, 2);
    }

    function getPerimeter()
    {
        return round(2 * $this->radius * self::PI, 2);
    }

}

$circle = new Circle();
$circle->inputRadius();
echo 'The Area: ' . $circle->getArea() . "\n";
echo 'The Perimeter: ' . $circle->getPerimeter() . "\n";
