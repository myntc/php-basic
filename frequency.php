<?php
/**
 * Author: My Nguyen Thi Chi
 */

$inputFile = fopen('input.txt', 'r') or die('Unable to open file!'); // Open the input file
$inputArray = preg_split('/[\s,]+/', fread($inputFile, filesize('input.txt'))); // Read the input file and convert the input to the array
$count = array_count_values($inputArray); // Count the frequency of words
$result = [];
foreach ($count as $key => $value) {
    if ($value >= 3) {
        $result[] = $key;
    }
}
echo 'The word(s) repeated more than 3 times: ' . implode(',', $result) . "\n";
